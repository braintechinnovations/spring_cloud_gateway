package com.lez21.springmsgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMsGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMsGatewayApplication.class, args);
	}

}
