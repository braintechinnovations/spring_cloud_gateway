Per creare un cloud:

1. Creiamo i microservizi di riferimento, possiamo utilizzare
o l'application.properties (che già sapete usare) oppure
"application.yaml" che si differenzia dal suo predecessore
dal formato che dà molta importanza all'indent ed il separatore
delle proprietà è identificato da ":"

Esempio sui microservizi:

#MS Studenti
spring:
  application:
    name: servizio-studenti
server:
  port: 8082
  
#MS Libri
spring:
  application:
    name: servizio-libro
server:
  port: 8081
  
A questo punto create un Spring Project con plugin "Gateway"
All'interno del gateway specificate il file "application.yaml" con
le seguenti strutture di Routing!

server:
  port: 8080
spring:
  cloud:
    gateway:
      routes:
      - id: moduloLibri
        uri: http://localhost:8081
        predicates:
        - Path=/libri/**
      - id: moduloStudenti
        uri: http://localhost:8082
        predicates:
        - Path=/studenti/**
#        - Path=/studenti/test, /studenti/insert

L'ultima parte commentata serve a filtrare i "segment" da trasferire
al microservizio, in queto caso possono passare (vengono trasferiti)
solo /studenti/test e /studenti/insert.